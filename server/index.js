const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({ port: 6759 });

let texts = 'Initial Data';

wss.on('connection', (ws) => {
    console.log('WebSocket connection!');

    ws.on('message', (event) => {

        console.log('event: ', event);
        const eventData = new Buffer.from(event).toString()
        console.log('eventData: ', eventData);
        ws.send(JSON.stringify(eventData));
    });

    ws.send(JSON.stringify({data: 'Initial data'}));

    ws.on('close', () => {
        console.log('disconnected');
    });

});
