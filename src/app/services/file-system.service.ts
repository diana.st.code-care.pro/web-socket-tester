import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TerminalService } from './terminal.service';

@Injectable({
  providedIn: 'root'
})
export class FileSystemService {

  public fileName: string = '';
  public processRunning: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private terminalService: TerminalService
  ) { }

  //get file
  private getFile(id: string){
    let control: HTMLInputElement = <HTMLInputElement>document.getElementById(id);
    let files = control.files || [];
    this.fileName = files[0].name
    return files[0];
  }

  //get data from file and return Promise
  private getDataFile(file: Blob): Promise<string>{
    this.processRunning.next(true);

    return new Promise((resolve, reject) => {
      this.terminalService.writeToConsole(`urls start downloading from ${this.fileName}...`);
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      if (!['text/plain', 'application/json'].includes(file.type)){
        return reject(reader.error?.code);
      }
      reader.onload = () => {
        this.processRunning.next(false);

        let contents: string = reader.result as string;
        resolve(contents);
      };
      reader.onerror = () => {
        this.processRunning.next(false);

        reject(reader.error?.code);
      };
    })
  }

  // load urls from file and save
  public getDataFromFile(id: string){
    this.getDataFile(this.getFile(id)).then(
      data => {
        this.terminalService.clearConsole();
        this.terminalService.writeToConsole(data);
      },
      error => {
        this.terminalService.writeToConsole('format not supported select file in .txt format');
      }
    )
  }
}
