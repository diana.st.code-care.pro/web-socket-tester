import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TerminalService {

  public consoleInfo$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() {
  }

  public writeToConsole(text: string) {
    this.consoleInfo$.next(text);
  }

  public clearConsole() {
    this.consoleInfo$.next('');
  }

}
