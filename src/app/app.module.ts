import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { WebsocketModule } from './websocket';
import { environment } from '../environments/environment';
import { DataSenderComponent } from './components/data-sender/data-sender.component';
import { DataReceiverComponent } from './components/data-receiver/data-receiver.component';

@NgModule({
  declarations: [
    AppComponent,
    DataSenderComponent,
    DataReceiverComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    WebsocketModule.config({
      url: environment.ws
  })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
