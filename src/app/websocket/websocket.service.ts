import { Injectable, OnDestroy, Inject } from '@angular/core';
import { Observable, SubscriptionLike, Subject, Observer, interval } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WebSocketSubject, WebSocketSubjectConfig } from 'rxjs/webSocket';

import { share, distinctUntilChanged, takeWhile } from 'rxjs/operators';
import { IWebsocketService, IWsMessage, WebSocketConfig } from './websocket.interfaces';
import { config } from './websocket.config';


@Injectable({
    providedIn: 'root'
})
export class WebsocketService implements IWebsocketService, OnDestroy {

    private config: WebSocketSubjectConfig<IWsMessage<any>>;

    private websocketSub: SubscriptionLike;
    private statusSub: SubscriptionLike;

    private reconnection$!: Observable<number> | null;
    private websocket$!: WebSocketSubject<any> | null;
    private connection$!: Observer<boolean>;
    private wsMessages$: Subject<IWsMessage<any>>;

    private reconnectInterval: number;
    private reconnectAttempts: number;
    private isConnected!: boolean;


    public status: Observable<boolean>;

    constructor(@Inject(config) private wsConfig: WebSocketConfig) {
        this.wsMessages$ = new Subject<IWsMessage<any>>();

        this.reconnectInterval = wsConfig.reconnectInterval || 5000; // pause between connections
        this.reconnectAttempts = wsConfig.reconnectAttempts || 10; // number of connection attempts

        this.config = {
            url: wsConfig.url,
            closeObserver: {
                next: (event: CloseEvent) => {
                    this.websocket$ = null;
                    this.connection$.next(false);
                }
            },
            openObserver: {
                next: (event: Event) => {
                    console.log('WebSocket connected!');
                    this.connection$.next(true);
                }
            },
            serializer: (data: any) => data
        };

        // connection status
        this.status = new Observable<boolean>((observer) => {
            this.connection$ = observer;
        }).pipe(share(), distinctUntilChanged());

        // run reconnect if not connection
        this.statusSub = this.status
            .subscribe((isConnected) => {
                this.isConnected = isConnected;

                if (!this.reconnection$ && typeof(isConnected) === 'boolean' && !isConnected) {
                    this.reconnect();
                }
            });

        this.websocketSub = this.wsMessages$.subscribe(
            {error: (error: ErrorEvent) => console.error('WebSocket error!', error)}
        );

        this.connect();
    }

    ngOnDestroy() {
        this.websocketSub.unsubscribe();
        this.statusSub.unsubscribe();
    }


    /*
    * connect to WebSocked
    * */
    private connect(): void {
        this.websocket$ = new WebSocketSubject(this.config);

        this.websocket$.subscribe({
            next: (message) => this.wsMessages$.next(message),
            error: (error: Event) => {
                console.log('err: ', error);
                if (!this.websocket$) {
                    // run reconnect if errors
                    this.reconnect();
                }
            }
        })
    }


    /*
    * reconnect if not connecting or errors
    * */
    private reconnect(): void {
        this.reconnection$ = interval(this.reconnectInterval)
            .pipe(takeWhile((v, index) => index < this.reconnectAttempts && !this.websocket$));

        this.reconnection$.subscribe({
            next: (v) => this.connect(),
            error: (e) => console.error(e),
            complete: () => {
               // Subject complete if reconnect attemts ending
                this.reconnection$ = null;

                if (!this.websocket$) {
                    this.wsMessages$.complete();
                    this.connection$.complete();
                }
            }
        })
    }


    /*
    * on message event
    * */
    public on<T>(event?: string): Observable<T> {
        if (event) {
            return this.wsMessages$.pipe(
                filter((message: IWsMessage<T>) => message.event === event),
                map((message: IWsMessage<T>) => message.data)
            );
        }
        else return this.wsMessages$.pipe(
            map((message: IWsMessage<T> | any) => {
                console.log('received data: ', message);
                let messageData = message?.data ? message?.data : message;
                return this.checkIsBase65(messageData)
                        ? this.decodeBase64(messageData)
                        : this.isJson(messageData)
                            ? JSON.parse(messageData)
                            : messageData

            })
        );
    }


    /*
    * on message to server
    * */
    public send(data: any = {}, setBtoa: boolean = true, event?: string): boolean {
        if(!this.isConnected || !this.websocket$) {
            console.error('Send error!');
            return false;
        }
        if (event) {
            this.websocket$.next(<any>JSON.stringify({ event, data }));
            return true;
        } else {
            let sendData: string;
            if(setBtoa) {
                sendData = this.encodeBase64(data);
                //sendData = btoa(data);
            }
            else {
                sendData = <any>JSON.stringify(data)
            }
            console.log('send data: ', sendData);
            this.websocket$.next(sendData);
            return true;
        }
    }

    /**
     * Determine if a String is a Base64
     */
    private checkIsBase65(value: string): boolean {
        const base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
        return base64regex.test(value)
    }

    private isJson(text: string): boolean {
        try {
          const json = JSON.parse(text);
          return true;
        } catch (e) {
          return false;
        }
    }

    private encodeBase64(text: string): string {
      return btoa(encodeURIComponent(text).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode(parseInt(p1, 16))
      }));
    }

    private decodeBase64(text: string): string {
      return decodeURIComponent(Array.prototype.map.call(atob(text), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
      }).join(''));
    }

}
