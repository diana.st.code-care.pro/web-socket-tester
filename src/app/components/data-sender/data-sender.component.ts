import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { WebsocketService } from 'src/app/websocket';
import { WS } from 'src/app/websocket.events';
import { environment } from 'src/environments/environment';
import { FileSystemService } from 'src/app/services/file-system.service';
import { TerminalService } from 'src/app/services/terminal.service';

@Component({
  selector: 'app-data-sender',
  templateUrl: './data-sender.component.html',
  styleUrls: ['./data-sender.component.scss']
})
export class DataSenderComponent implements OnInit, OnDestroy {

  @ViewChild('textarea') console!: ElementRef;

  public receiveConsoleVisible: boolean = environment.receiveConsoleVisible;
  public websoketUrl: string = environment.ws;

  public uploadingRunning!: BehaviorSubject<boolean>;

  private terminalSubscription$!: Subscription;

  constructor(
    private fileSystemService: FileSystemService,
    private terminalService: TerminalService,
    private wsService: WebsocketService
  ) {
    this.uploadingRunning = this.fileSystemService.processRunning;
   }

  ngOnInit(): void {
    setTimeout(() => {
      this.initTerminalSubscriptions();
    }, 0);
  }

  private initTerminalSubscriptions() {
    this.terminalSubscription$ = this.terminalService.consoleInfo$.subscribe((data) => {
      this.writeToTextbox(data);
    });
  }

  public download(id: string): void{
    this.fileSystemService.getDataFromFile(id)
  }

  private writeToTextbox(text: string) {
    this.console.nativeElement.value = text;
  }

  public clearTextbox() {
    this.console.nativeElement.value = '';
  }

  private get consoleData() {
    return this.console.nativeElement.value;
  }

  public sendText(): void {
    if (!this.consoleData) {
      this.terminalService.writeToConsole(`You didn't entered the data.`);
      return;
    }

    if (!this.isJson(this.consoleData)) {
      this.terminalService.writeToConsole(`Text is not JSON. You can send only JSON to the WebSocket.`);
      return;
    }

    const rezult = this.wsService.send(this.consoleData);

    if (rezult) {
      this.terminalService.writeToConsole(`Succesfully sent data to the ${this.websoketUrl} WebSocket`);
    } else {
      this.terminalService.writeToConsole(`Error! Check whether connection to ${this.websoketUrl} WebSocket is active`);
    }
  }

  private isJson(text: string): boolean {
    try {
      const json = JSON.parse(text);
      return true;
    } catch (e) {
      return false;
    }
  }

  ngOnDestroy(): void {
    if(this.terminalSubscription$) {
      this.terminalSubscription$.unsubscribe()
    }
  }

}
