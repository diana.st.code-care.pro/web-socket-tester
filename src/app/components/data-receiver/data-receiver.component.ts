import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { WebsocketService } from 'src/app/websocket';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-data-receiver',
  templateUrl: './data-receiver.component.html',
  styleUrls: ['./data-receiver.component.scss']
})
export class DataReceiverComponent implements OnInit, OnDestroy {

  @ViewChild('receive_textarea') receiveConsole!: ElementRef;

  private messages$!: Observable<any>;

  private dataFromSocket$!: Subscription;

  public receiveConsoleVisible: boolean = environment.receiveConsoleVisible;
  public websoketUrl: string = environment.ws;

  constructor(
    private wsService: WebsocketService
  ) { }

  ngOnInit(): void {
    // get messages
    this.messages$ = this.wsService.on<any>();
    this.dataFromSocket$ = this.messages$.subscribe({
      next: (d) => {
        if(this.receiveConsoleVisible) {
          this.receiveConsole.nativeElement.value += d + '\n';
        }
        this.scrollReceiveConsole();
        console.log('data from socket: ', d);
      }
    })
  }

  public clearReceiveTextbox() {
    this.receiveConsole.nativeElement.value = '';
  }

  private scrollReceiveConsole() {
    this.receiveConsole.nativeElement.scrollTop = this.receiveConsole.nativeElement.scrollHeight;
  }

  ngOnDestroy(): void {
    if(this.dataFromSocket$) {
      this.dataFromSocket$.unsubscribe()
    }
  }
}
