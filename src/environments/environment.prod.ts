export const environment = {
  production: true,
  ws: 'ws://127.0.0.1:8105/printing',
  receiveConsoleVisible: true
};
